#!/usr/bin/env bash

# Examle start install roling distribution
# "opensuse tumbleweed". Change option if you want.

virt-install \
    --connect xen:/// \
    --virt-type xen  \
    --hvm --name ost \
    --vcpus 1 \
    --memory 2048 \
    --disk path=./root.img,size=5 \
    --location 'https://download.opensuse.org/tumbleweed/repo/oss/' \
    --graphics none \
    --console pty,target_type=serial \
    --extra-args 'console=ttyS0,115200n8 serial' \
    --os-type linux \
    --os-variant generic \
    --network bridge=br0
