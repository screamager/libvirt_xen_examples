# libvirt_xen_examples
Start install OS use virt-install.
This is example start install linux OS into libvirt using hipervisor XEN.

The examples use openSUSE Leap 15 and openSUSE Tumbleweed.

NOTE: Disk image cans create not only "size=5" (5 is 5Gbyte)
but and comands: truncate or qemu-img
```
$ truncat -s 5G root.img # it's raw image
$ qemu-img create -f qcow2 -o size=5G ./root.img
```

After install start virt domain and connect console:
```
$ virtsh start <domain name>
$ virtsh console <domain name>
```

If when installing happend fails and you want delete domain then:
```
$ virtsh shoutdown <domain name>
$ virtsh destroy <domain name>
$ virtsh undefine --domain <domain name>
```

Show status domain:
```
$ virtsh list
$ virtsh list --all # for full list
```
