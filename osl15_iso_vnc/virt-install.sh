#!/usr/bin/env bash

# If you ran this script on local machin or
# connect to server with forwarding X11 
# '$ ssh -X <user>@<host adress>'
# then into "graphics delete parameter listen.
# Then a windows with vnc connect auto open.

virt-install \
    --connect xen:/// \
    --virt-type xen  \
    --hvm --name osl15 \
    --vcpus 1 \
    --memory 2048 \
    --disk path=./root.img \
    --cdrom .//openSUSE-Leap-15.0-DVD-x86_64.iso \
    --graphics vnc,listen=0.0.0.0,port=5999 \
    --console pty,target_type=serial \
    --os-type linux \
    --os-variant generic \
    --network bridge=br0
