#!/usr/bin/env bash

# Example for install OS into text mode.
# Change name, path and etc if you want.
# Option "hvm" - full virtualization
# If you want set ip address than add into
# "extra-args" ( this parameter work with "location") suchlike
# "ip=192.168.1.2::192.168.1.1:255.255.255.0:test.example.com:eth0:none"

virt-install \
    --connect xen:/// \
    --virt-type xen  \
    --hvm --name osl15 \
    --vcpus 1 \
    --memory 2048 \
    --disk path=./root.img,size=5 \
    --location ./openSUSE-Leap-15.0-DVD-x86_64.iso \
    --graphics none \
    --console pty,target_type=serial \
    --extra-args 'console=ttyS0,115200n8 serial' \
    --os-type linux \
    --os-variant generic \
    --network bridge=br0
